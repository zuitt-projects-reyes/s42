// alert("hello")

console.log(document);

// in this line of code we are getting the document in the HTML element with the id txt-first-name
const txtFirstName = document.querySelector("#txt-first-name")

// alternative way of targeting an element
// document.getElementById("txt-first-name")
// document.getElementByClassName()
// document.getElementByTagName()

/*
	Mini Activity

	1. Target the full name element and store it in a constant called spanFullName
*/

const spanFullName = document.querySelector("#span-full-name")



txtFirstName.addEventListener('keyup', (event) => {

	spanFullName.innerHTML = txtFirstName.value

});

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value);
});


// Get the last name field
const txtLastName = document.querySelector("#txt-last-name");

// Update the contents of the full name field with the value of the first and last name
function updateFullName() {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
};

// Trigger the callback when the last name field changes
txtLastName.addEventListener('keyup', updateFullName);
